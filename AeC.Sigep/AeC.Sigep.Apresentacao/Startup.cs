﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AeC.Sigep.Apresentacao.Startup))]
namespace AeC.Sigep.Apresentacao
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
