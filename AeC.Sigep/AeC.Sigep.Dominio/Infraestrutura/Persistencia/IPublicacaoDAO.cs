﻿using AeC.Sigep.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeC.Sigep.Dominio.Infraestrutura.Persistencia
{
    public interface IPublicacaoDAO
    {
        void Insere(Publicacao publicacao);

        void Edita(Publicacao publicacao);

        void Deleta(Publicacao publicacao);

        IEnumerable<Publicacao> Busca(Publicacao publicacao);
    }
}
