﻿using AeC.Sigep.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeC.Sigep.Dominio.Infraestrutura.Persistencia
{
    public interface IIntegracaoDAO
    {
        void Insere(Integracao integracao);

        void Edita(Integracao integracao);

        void Deleta(Integracao integracao);

        IEnumerable<Publicacao> Busca(Integracao integracao);
    }
}
