﻿using AeC.Sigep.Dominio.Entidades;
using System.Collections;
using System.Collections.Generic;

namespace AeC.Sigep.Dominio.Infraestrutura
{
    public interface IAplicacaoDAO
    {
        void Insere(Aplicacao aplicacao);

        void Edita(Aplicacao aplicacao);

        void Deleta(Aplicacao aplicao);

        IEnumerable<Aplicacao> Busca(Aplicacao aplicacao);
    }
}
