﻿using System;

namespace AeC.Sigep.Dominio
{
    public interface IInjecaoDependencia
    {
        T Resolve<T>();

        T Resolve<T>(Type type);
    }
}
