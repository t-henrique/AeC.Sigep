﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AeC.Sigep.Dominio.Entidades
{
    public class Publicacao
    {
        [Editable(false)]
        public virtual int Id { get; set; }

        [DisplayName("Link Interno:")]
        [Required(ErrorMessage = @"Informe ao menos o link interno da Aplicação.")]
        [MaxLength(250, ErrorMessage = @"O campo máximo permitido para a Url é de 250 caractéres.")]
        [Url(ErrorMessage = @"Favor insira uma URL válida.")]
        public virtual string UrlInterna { get; set; }

        [DisplayName("Link Externo:")]
        [MaxLength(250, ErrorMessage = @"O campo máximo permitido para a Url é de 250 caractéres.")]
        [Url(ErrorMessage = @"Favor insira uma URL válida.")]
        public virtual string UrlExterna { get; set; }

        [DisplayName("Ambiente: ")]
        [Required(ErrorMessage = @"Ambiente precisa ser selecionado.")]
        public virtual EnumAmbiente Ambiente { get; set; }

        [DisplayName("Nome do Servidor:")]
        public virtual string Servidor { get; set; }

        [DisplayName("Última publicação")]
        [ReadOnly(true)]
        public virtual DateTime UltimaPublicacao { get; set; }
    }
}
