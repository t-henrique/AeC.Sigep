﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AeC.Sigep.Dominio.Entidades
{
    public class Aplicacao
    {
        [DisplayName("Nome do Sistema:")]
        public virtual string Nome { get; set; }

        [DisplayName("Versão:")]
        [Required(ErrorMessage = "Informe a versão.")]
        [RegularExpression(@"(?:(^[v]\d+\.\d+\.\d+\.\d{4}))$", ErrorMessage = @"Versão inválida, utilize entre os dois padrões possíveis: v0.0.0.0000.")]
        [StringLength(20, ErrorMessage = @"Limite máximo 20 caracteres.")]
        public virtual string Versao { get; set; }

        [Editable(false)]
        public virtual int Id { get; set; }

        public virtual Publicacao Publicacao { get; set; }

        public virtual Integracao Integracao { get; set; }
    }
}
