﻿using System.ComponentModel;

namespace AeC.Sigep.Dominio.Entidades
{
    public enum EnumAmbiente
    {
        [Description("Teste")]
        Teste = 1,

        [Description("Homologação")]
        Homologacao = 2, 

        [Description("Produção")]
        Producao = 3
    }
}
