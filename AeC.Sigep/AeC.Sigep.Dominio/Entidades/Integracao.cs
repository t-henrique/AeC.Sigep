﻿
using System.ComponentModel;
namespace AeC.Sigep.Dominio.Entidades
{
    public class Integracao
    {
        public virtual int Id { get; set; }

        [DisplayName("Aplicacao Consumidora:")]
        public virtual Aplicacao AplicacaoSolicitanteId { get; set; }

        [DisplayName("Aplicacao Fornecedora:")]
        public virtual Aplicacao AplicacaoFornecedoraId { get; set; }
    }
}
