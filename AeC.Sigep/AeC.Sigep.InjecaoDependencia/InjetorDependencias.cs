﻿using AeC.Sigep.Dominio;
using AeC.Sigep.Dominio.Infraestrutura;
using AeC.Sigep.Dominio.Infraestrutura.Log;
using AeC.Sigep.Dominio.Infraestrutura.Persistencia;
using AeC.Sigep.Infraestrutura;
using AeC.Sigep.Infraestrutura.Log;
using AeC.Sigep.Infraestrutura.Persistencia;
using Microsoft.Practices.Unity;
using System;

namespace AeC.Sigep.InjecaoDependencia
{
    public class InjetorDependencias : IInjecaoDependencia
    {
        private readonly IUnityContainer _container;
        private static InjetorDependencias _injetorDependencia;

        private InjetorDependencias()
        {
            _container = new UnityContainer();
            MapearDependencias();
        }

        public static InjetorDependencias Instancia()
        {
            return _injetorDependencia ?? (_injetorDependencia = new InjetorDependencias());
        }

        public T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        public T Resolve<T>(Type type)
        {
            return (T)_container.Resolve(type);
        }

        private void MapearDependencias()
        {
            _container
                .RegisterType<IAplicacaoDAO, AplicacaoDAO>()
                .RegisterType<IPublicacaoDAO, PublicacaoDAO>()
                .RegisterType<IIntegracaoDAO, IntegracaoDAO>()
                .RegisterType<ILog, Log>();
        }
    }
}
