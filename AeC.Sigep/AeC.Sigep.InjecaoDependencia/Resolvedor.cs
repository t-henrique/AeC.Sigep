﻿using AeC.Sigep.Dominio;
using System;

namespace AeC.Sigep.InjecaoDependencia
{
    public class Resolvedor
    {
        private static IInjecaoDependencia _injecao;

        public static void Inicializar(IInjecaoDependencia injetor)
        {
            _injecao = injetor;
        }

        public static T Resolver<T>()
        {
            return _injecao.Resolve<T>();
        }

        public static T Resolver<T>(Type type)
        {
            return _injecao.Resolve<T>(type);
        }
    }
}
