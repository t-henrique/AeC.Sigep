# SIGEP - Sistema de Gestão de Publicação. #
Sistema designado para a gestão de publicação dos sistemas da Aec. Seus objetivos estão limitados apenas ao cadastro de sistemas, seus respectivos servidores, versões e links de acessos publicados. 

Este sistema tem como objetivo reunir todas as informações de publicação existente na área de sistemas para que fique fácil identificar e reunir dados sobre o paradeiro dos sistemas desenvolvidos. 

## Escopo

O Sigep permitirá apenas aos usuários autenticados o cadastro e a manutenção das informações dos sistemas disponíveis e disponibilizará aos usuários não autenticados uma view para conferir as informações cadastradas.

O Sigep integrará com o Autentica API e não há necessidade de integração de mais nenhum outro sistema.

O Sigep também será capaz de consolidar dados sobre as dependências e integrações das aplicações. O termo dependência se dá a aplicação 'A' que necessita de um serviço, informação ou dado de uma certa aplicação, B, que fornece. 

O Sigep consolidará relatórios que vai expor a quantidade de sistemas por servidor, relação dependência por aplicação / integração e aplicações existentes.

## Funcionalidades

Todas as funcionalidades de cadastro só poderão ser realizadas se o usuário estiver autenticado no sistema.



 

 